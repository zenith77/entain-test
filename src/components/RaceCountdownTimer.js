import React, { useState, useEffect } from "react";

function calcDiffInSeconds(dateA, dateB) {
  return Math.floor(((dateA.getTime() - dateB.getTime()) / 1000)); // TODO CALCULATIONS HERE
}

const RaceCountdownTimer = (props) => {
  const dateFrom = props.dateFrom;
  const [currentDate, setCurrentDate] = useState(new Date());
  const [secondsDiff, setSecondsDiff] = useState(
    calcDiffInSeconds(dateFrom, currentDate)
  );

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      setCurrentDate(new Date());
    }, 1000);

    return () => clearTimeout(timeoutId);
  }, [currentDate]);

  const triggerUpdateHandler = props.triggerUpdateHandler;

  useEffect(() => {
    const newSecondsDiff = calcDiffInSeconds(dateFrom, currentDate);
    setSecondsDiff(calcDiffInSeconds(dateFrom, currentDate));
    if (newSecondsDiff < -100) {
      triggerUpdateHandler();
    }
  }, [currentDate, dateFrom, triggerUpdateHandler]);

  return secondsDiff >= 61 ? (
    <div className="race-countdown-to-go">
      {secondsDiff}
      <span className="race-countdown-text">secs. to go</span>
    </div>
  ) : secondsDiff >= 0 ? (
    <div className="race-countdown-to-go-urgent">
      {secondsDiff}
      <span className="race-countdown-text">secs. to go</span>
    </div>
  ) : (
    <div className="race-countdown-since-start">
      {-secondsDiff}
      <span className="race-countdown-text">since start</span>
    </div>
  );
}

export default RaceCountdownTimer;