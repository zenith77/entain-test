
const GREYHOUND_RACING_ID = '9daef0d7-bf3c-4f50-921d-8e818c60fe61';
const HARNESS_RACING_ID = '161d9be2-e909-4326-8c2c-35ed71fb460b';
const HORSE_RACING_ID = '4a2788f8-e825-4d36-9894-efd4baf1cfae';

const RaceCategoryFilter = (props) => {
  return (
    <form>
      <select onChange={props.onChangeEvent} className="category-filter">
        <option value="0">All Races</option>
        <option value={GREYHOUND_RACING_ID}>Greyhound Racing</option>
        <option value={HARNESS_RACING_ID}>Harness Racing</option>
        <option value={HORSE_RACING_ID}>Horse Racing</option>
      </select>
    </form>
  );
};

export default RaceCategoryFilter;