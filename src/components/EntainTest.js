import React from "react";
import RaceCategoryFilter from "./RaceCategoryFilter";
import RaceCountdownTimer from "./RaceCountdownTimer";

const RACE_VIEWS = 5;
const RACES_RETRIEVED = 25;

// function calcDiffInSeconds(dateA, dateB) {
//   return Math.floor((dateA.getTime() - dateB.getTime()) / 1000); // TODO CALCULATIONS HERE
// }

class EntainTest extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      races: [],
      category: "0",
    };
    this.handleFilterChange = this.handleFilterChange.bind(this);
    this.triggerUpdateHandler = this.triggerUpdateHandler.bind(this);
  }

  componentDidMount() {
    fetch(
      "https://api.neds.com.au/rest/v1/racing/?method=nextraces&count=" +
        RACES_RETRIEVED
    )
      .then((res) => res.json())
      .then(
        (result) => {
          // console.log("calling api")
          console.log(result.data);
          let racesArray = [];
          let index = 0;
          result.data.next_to_go_ids.forEach((element) => {
            racesArray[index++] = result.data.race_summaries[element];
          });
          this.setState({
            isLoaded: true,
            races: racesArray,
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      );

    // var intervalId = setInterval(this.timer, 1000);
    // this.setState({ intervalId: intervalId });
  }

  // componentWillUnmount() {
  //   clearInterval(this.state.intervalId);
  // }

  // timer() {
  //   setSecondsDiff(calcDiffInSeconds(dateFrom, currentDate));
  // }

  handleFilterChange(event) {
    this.setState({
      category: event.target.value,
    });
  }

  triggerUpdateHandler() {
    console.log("forceUpdate");
    this.forceUpdate();
  }

  render() {
    const { error, isLoaded, races } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      const sortedRaces = races
        .filter((element) => {
          return this.state.category === "0"
            ? true
            : element.category_id === this.state.category;
        })
        .filter((element) => {
          return (new Date().getTime() / 1000) - 100 < (element.advertised_start.seconds);
        })
        .filter((element, index) => {
          return index < RACE_VIEWS;
        })
        .sort((elementOne, elementTwo) => {
          return elementOne.advertised_start.seconds >
            elementTwo.advertised_start.seconds
            ? 1
            : -1;
        });
      return (
        <div>
          <RaceCategoryFilter onChangeEvent={this.handleFilterChange} />
          <ul>
            {sortedRaces.map((race, index) => (
              <div key={race.race_id}>
                <div className="race-card">
                  <div className="race-meet">{race.meeting_name}</div>
                  <div className="race-date">
                    {new Date(
                      race.advertised_start.seconds * 1000
                    ).toLocaleString("en-AU")}
                  </div>
                </div>
                <div className="race-number">{race.race_number}</div>
                <RaceCountdownTimer
                  dateFrom={new Date(race.advertised_start.seconds * 1000)}
                  triggerUpdateHandler={this.triggerUpdateHandler}
                />
              </div>
            ))}
          </ul>
        </div>
      );
    }
  }
}

export default EntainTest;
